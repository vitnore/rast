# Rast – Web File Manager
![screenshot](https://i.imgur.com/aVFRmig.jpg)

## Build Setup
```
# Install API dependencies (root folder)
npm i

# Start server
npm run server

# Install app dependencies (/app/ folder)
npm i

# Start app at localhost:8080
npm run dev
```