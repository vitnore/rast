module.exports = (app) => {
  const api = app.RastAPI.app.api.folder;

  app.route('/')
    .get((req, res) => res.send('Rast API'));
  app.route('/api/folder')
    .post(api.getFolder);
  app.route('/api/copy')
    .post(api.copyItems);
  app.route('/api/move')
    .post(api.moveItems);
  app.route('/api/delete')
    .post(api.deleteItems);
}